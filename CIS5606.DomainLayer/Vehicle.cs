﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DomainLayer
{
    public class Vehicle<T1, T2> //GENERIC CLASS <T1 = INT, T2 = STRING>
    {
        //properties
        public T1 Id { get; set; }
        public T2 Vin { get; set; }
        public T2 Make { get; set; }
        public T2 Model { get; set; }
        public T1 Year { get; set; }
        public T2 Color { get; set; }
        public T1 Capacity { get; set; }
        public T1 Mileage { get; set; }
        public T2 Type { get; set; }

        //ctor 
        public Vehicle(T1 id, T2 vin, T2 make, T2 model, T1 year, T2 color, T1 capacity, T1 mileage, T2 type)
        {
            //initializing properties 
            Id = id;
            Vin = vin;
            Make = make;
            Model = model;
            Year = year;
            Color = color;
            Capacity = capacity;
            Mileage = mileage;
            Type = type;
            
        } //end ctor

        ////ctor for creating an instance
        //public Vehicle(string make, string model, int year, string color, int capacity, int mileage, string type)
        //{
        //    //So once the instance is passed to the Business layer, the business layer creates the id (As this is created automatically by DB)
        //    //initializing properties 
        //    Make = make;
        //    Model = model;
        //    Year = year;
        //    Color = color;
        //    Capacity = capacity;
        //    Mileage = mileage;
        //    Type = type;

        //} //end ctor

        //ToString -- OVERRIDEN
        public override string ToString()
        {
            return $"Vehicle Information: ID: {Id} MAKE: {Make} MODEL: {Model} YEAR: {Year} COLOR: {Color} CAPACITY: {Capacity} MILEAGE: {Mileage} TYPE: {Type}";
        } //end overriden ToString()
    }
}
