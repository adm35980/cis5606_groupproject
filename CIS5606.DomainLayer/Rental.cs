﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DomainLayer
{
    public class Rental<T1, T2> //GENERIC CLASS <T1 = INT, T2= DATE>
    {
        //properties
        public T1 Id { get;}
        public T2 RentalDate { get; set; }
        public T1 Duration { get; set; }
        public T1 VehicleId{ get; set; }
        public T1 CustomerId { get; set; }
        public T1 EmployeeId { get; set; }

        //ctor 
        public Rental(T1 id, T2 rentalDate, T1 duration, T1 vehicleId, T1 customerId, T1 employeeId)
        {
            //initializing properties 
            Id = id;
            RentalDate = rentalDate;
            Duration = duration;
            VehicleId = vehicleId;
            CustomerId = customerId;
            EmployeeId = employeeId;

        } //end ctor

        //ToString -- OVERRIDEN
        public override string ToString()
        {
            return $"Rental Information: ID: {Id} RentalDate: {RentalDate} Duration: {Duration} VehicleId: {VehicleId} CustomerId: {CustomerId} EmployeeId: {EmployeeId}";
        } //end overriden ToString()
    }
}
  