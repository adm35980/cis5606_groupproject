﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DomainLayer
{
    public class Customer<T1, T2>:User<T1,T2> //GENERIC CLASS <T1 = INT, T2 = STRING>
    {
        //properties
        public T2 Address { get; set; }

        //ctor 
        public Customer(T1 id, T2 fName, T2 lName, T2 phone, T2 email, T2 address):base(id, fName,lName,phone,email)
        {
            //initializing properties 
            Address = address;

        } //end ctor

        //ToString -- OVERRIDEN
        public override string GetUserInfo()
        {
            return $"Customer Information: ID: {Id} First Name: {FirstName} Last Name: {LastName} " +
                $"Phone: {Phone} Email: {Email} Address: {Address} ";
        } //end overriden ToString()
    }
}
