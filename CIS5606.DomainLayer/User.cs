﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DomainLayer
{
    public abstract class User<T1,T2> //GENERIC CLASS <T1 = INT, T2 = STRING>
    {
        //properties
        public T1 Id { get; }
        public T2 FirstName { get; set; }
        public T2 LastName { get; set; }
        public T2 Phone { get; set; }
        public T2 Email { get; set; }

        //ctor 
        public User(T1 id, T2 fName, T2 lName, T2 phone, T2 email)
        {
            //initializing properties 
            Id = id;
            FirstName = fName;
            LastName = lName;
            Phone = phone;
            Email = email;

        } //end ctor

        public abstract string GetUserInfo();
    }
}
