﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DataAccessLayer;

namespace CIS5606.BusinessLayer
{
    public class CustomerBL
    {
        //SQL QUERIES
        private string selectAllInfo = "SELECT Id, First_Name, Last_Name, Phone, Email, Address FROM CUSTOMER;";
        private string insertOneRecord = "INSERT INTO CUSTOMER (First_Name, Last_Name, Phone, Email, Address)" +
                                        "VALUES (@firstNameParm, @lastNameParm, @phoneParm, @emailParm, @addressParm);";
        private string updateOneRecord = "UPDATE CUSTOMER SET First_Name = @firstNameParm, Last_Name = @lastNameParm, Phone = @phoneParm, Email = @emailParm, Address = @addressParm WHERE Id = @idParm";
        private string deleteOneRecord = "DELETE FROM CUSTOMER WHERE Id = @idParm;";


        //DATA ACCESS CLASS INSTANCE
        CustomerDAC customerDAC = new CustomerDAC();

        //Error Exception Classes Instance
        BLException blException = new BLException();
        DACException dacException = new DACException();

        public DataTable FetchData()
        {
            try
            {
                //DataSet that save retrieve information -- RetrieveData() in BaseAccessClass
                DataSet customersData = customerDAC.RetrieveData(selectAllInfo);

                //return data in dataSet to the form
                return customersData.Tables[0];
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return null;
            } //end try-catch 

        } //end FetchData() -- Retrieves ALL vehicle information in the database

        public int InsertNewRecord(object obj)
        {
            /*
             * ACTION TAKEN:
             * - WILL RECEIVE AN OBJECT FROM THE FORM
             * - WILL PASS THAT OBJECT & THE INSERT QUERY TO THE DATA ACCESS CLASS
             */

            try
            {
                return customerDAC.InsertOneRecord(insertOneRecord, obj);
            }
            catch(Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return 0;
            }


        } //end InsertNewRecord()

        public int UpdateOneRecord(object obj)
        {
            try
            {
                return customerDAC.UpdateOneRecord(updateOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return 0;
            }
        }

        public int DeleteOneRecord(object obj)
        {
            /*
             * ACTION TAKEN:
             * - WILL RECEIVE AN OBJECT FROM THE FROM
             * - WILL PASS THAT OBJECT & THE DELETE QUERY TO THE DATA ACCESS CLASS
             */
            try
            {
                return customerDAC.DeleteOneRecord(deleteOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return 0;
            }
        } //end DeleteOneRecord()
    }
}
