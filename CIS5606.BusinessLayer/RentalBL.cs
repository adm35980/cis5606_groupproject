﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DataAccessLayer;

namespace CIS5606.BusinessLayer
{
    public class RentalBL
    {
        //SQL QUERIES
        private string selectAllInfo = "SELECT Id, Rental_Date, Duration, Vehicle_Id, Customer_Id, Employee_Id FROM RENTAL;";
        private string selectFromMultipleTables = "Select r.Id, r.Rental_Date, r.Duration, v.VIN, v.Make, v.Model, c.First_Name, c.Last_Name, e.First_Name, e.Last_Name FROM Rental r JOIN Vehicle v ON r.Vehicle_Id = v.Id JOIN Customer c ON r.Customer_Id = c.Id JOIN Employee e ON r.Employee_Id = e.Id;";
        private string insertOneRecord = "INSERT INTO RENTAL (Rental_Date, Duration, Vehicle_Id, Customer_Id, Employee_Id)" +
                                        "VALUES (@rentalDateParm, @durationParm, @vehicleIdParm, @customerIdParm, @employeeIdParm);";
        private string updateOneRecord = "UPDATE RENTAL SET Rental_Date = @rentalDateParm, Duration = @durationParm, Vehicle_Id = @vehicleIdParm WHERE ID = @idParm";
        private string deleteOneRecord = "DELETE FROM RENTAL WHERE Id = @idParm;";


        //DATA ACCESS CLASS INSTANCE
        RentalDAC rentalDAC = new RentalDAC();

        //Error Exception Classes Instance
        BLException blException = new BLException();
        DACException dacException = new DACException();

        public DataTable FetchData()
        {
            try
            {
                //DataSet that save retrieve information -- RetrieveData() in BaseAccessClass
                DataSet rentalsData = rentalDAC.RetrieveData(selectFromMultipleTables);

                //return data in dataSet to the form
                return rentalsData.Tables[0];
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return null;
            } //end try-catch 

        } //end FetchData() -- Retrieves ALL vehicle information in the database

        public int InsertNewRecord(object obj)
        {
            /*
             * ACTION TAKEN:
             * - WILL RECEIVE AN OBJECT FROM THE FORM
             * - WILL PASS THAT OBJECT & THE INSERT QUERY TO THE DATA ACCESS CLASS
             */

            try
            {
                return rentalDAC.InsertOneRecord(insertOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return Convert.ToInt32(null);
                throw;
            }
            

        } //end InsertNewRecord()

        public int UpdateOneRecord(object obj)
        {
            try
            {
                return rentalDAC.UpdateOneRecord(updateOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                Convert.ToInt32(null);
                throw;
            }
        }

        public int DeleteOneRecord(object obj)
        {
            /*
             * ACTION TAKEN:
             * - WILL RECEIVE AN OBJECT FROM THE FROM
             * - WILL PASS THAT OBJECT & THE DELETE QUERY TO THE DATA ACCESS CLASS
             */
            try
            {
                return rentalDAC.DeleteOneRecord(deleteOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return Convert.ToInt32(null);
            }

        } //end DeleteOneRecord()
    }
}
