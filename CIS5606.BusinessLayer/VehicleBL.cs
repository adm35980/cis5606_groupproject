﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DataAccessLayer;

namespace CIS5606.BusinessLayer
{
    public class VehicleBL
    {
        //SQL QUERIES -- STORED PROCEDURES
        private string selectAllInfo = "SELECT Id, VIN, Make, Model, Year, Color, Capacity, Mileage, Type FROM VEHICLE;";
        private string insertOneRecord = "INSERT INTO Vehicle (VIN, Make, Model, Year, Color, Capacity, Mileage, Type) VALUES (@vinParm, @makeParm, @modelParm, @yearParm, @colorParm, @capacityParm, @mileageParm, @vehicleTypeParm)";
        private string updateOneRecord = "UPDATE VEHICLE SET VIN = @vinParm, Make = @makeParm, Model = @modelParm, Year = @yearParm, Color = @colorParm, Capacity = @capacityParm, Mileage = @mileageParm, Type = @vehicleTypeParm WHERE Id = @idParm";
        private string deleteOneRecord = "DELETE FROM Vehicle WHERE Id = @idParm;";


        //DATA ACCESS CLASS INSTANCE
        VehicleDAC vehicleDAC = new VehicleDAC();

        //Error Exception Classes Instance
        BLException blException = new BLException();
        DACException dacException = new DACException();

        public DataTable FetchData()
        {
            try
            {
                //DataSet that save retrieve information -- RetrieveData() in BaseAccessClass
                DataSet vehiclesData = vehicleDAC.RetrieveData(selectAllInfo);

                //return data in dataSet to the form
                return vehiclesData.Tables[0];
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return null;
            } //end try-catch 

        } //end FetchData() -- Retrieves ALL vehicle information in the database

        public int InsertNewRecord(object obj)
        {
            /*
             * ACTION TAKEN:
             * - WILL RECEIVE AN OBJECT FROM THE FORM
             * - WILL PASS THAT OBJECT & THE INSERT QUERY TO THE DATA ACCESS CLASS
             */
            try
            {
                return vehicleDAC.InsertOneRecord(insertOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return Convert.ToInt32(null);
            }
            
        } //end InsertNewRecord()

        public int UpdateOneRecord(object obj)
        {
            try
            {
                return vehicleDAC.UpdateOneRecord(updateOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return Convert.ToInt32(null);
            }
        }

        public int DeleteOneRecord(object obj)
        {
            /*
             * ACTION TAKEN:
             * - WILL RECEIVE AN OBJECT FROM THE FROM
             * - WILL PASS THAT OBJECT & THE DELETE QUERY TO THE DATA ACCESS CLASS
             */

            try
            {
                return vehicleDAC.DeleteOneRecord(deleteOneRecord, obj);
            }
            catch (Exception ex)
            {
                blException.ErrorReturned = dacException.ErrorReturned + ex.Message;
                return Convert.ToInt32(null);
            }

        } //end DeleteOneRecord()
    }
}
