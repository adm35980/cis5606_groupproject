﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.BusinessLayer
{
    public class BLException : Exception
    {
        public String ErrorReturned { get; set; }

    }
}
