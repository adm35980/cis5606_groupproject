﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DomainLayer;

namespace CIS5606.DataAccessLayer
{
    public class VehicleDAC : BaseAccessClass, IDataAcces
    {
        //WILL USE FOR ERROR HANDLING
        DACException dacException = new DACException();

        //INTERFACE METHODS -- ADD IMPLEMENTATION
        public int InsertOneRecord(string query, object obj)
        {
            /*
             * ACTION TAKEN: 
             * - WILL RECEIVE AN OBJECT AND QUERY FROM INTERMEDIATE LAYER [BUSINESS LAYER]
             * - UPCAST THE PASSED IN OBJECT TO TYPE 'VEHICLE'
             * - WILL PERFORM THE INSERT INTO THE DATABASE
             * - WILL USE A SQLCOMMAND INSTEAD OF A DATA ADAPTER
             */

            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Vehicle<int, string> upcastedTempVehicle = (Vehicle<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                /*
                 * STEP 3: PERFORM INSERT INTO THE DATABASE
                 * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                 * - NEED TO OPEN CONNECTION TO THE DATABASE
                 * - NEED TO PERFORM THE INSERT
                 * - NEED TO CLOSE THE DATABASE
                 */

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND FOR INSERTION

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter vinParm = new SqlParameter("@vinParm", SqlDbType.VarChar);
                vinParm.Value = upcastedTempVehicle.Vin;

                SqlParameter makeParm = new SqlParameter("@makeParm", SqlDbType.VarChar);
                makeParm.Value = upcastedTempVehicle.Make;

                SqlParameter modelParm = new SqlParameter("@modelParm", SqlDbType.VarChar);
                modelParm.Value = upcastedTempVehicle.Model;

                SqlParameter yearParm = new SqlParameter("@yearParm", SqlDbType.Int);
                yearParm.Value = upcastedTempVehicle.Year;

                SqlParameter colorParm = new SqlParameter("@colorParm", SqlDbType.VarChar);
                colorParm.Value = upcastedTempVehicle.Color;

                SqlParameter capacityParm = new SqlParameter("@capacityParm", SqlDbType.Int);
                capacityParm.Value = upcastedTempVehicle.Capacity;

                SqlParameter mileageParm = new SqlParameter("@mileageParm", SqlDbType.Int);
                mileageParm.Value = upcastedTempVehicle.Mileage;

                SqlParameter vehicleTypeParm = new SqlParameter("@vehicleTypeParm", SqlDbType.VarChar);
                vehicleTypeParm.Value = upcastedTempVehicle.Type;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(vinParm);
                command.Parameters.Add(makeParm);
                command.Parameters.Add(modelParm);
                command.Parameters.Add(yearParm);
                command.Parameters.Add(colorParm);
                command.Parameters.Add(capacityParm);
                command.Parameters.Add(mileageParm);
                command.Parameters.Add(vehicleTypeParm);
                
                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery(); 
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                //CloseConnection(); - Need not be done cos it is handled by command.Dispose. CloseConnection()-only with adapter object
                command.Dispose();
            }

        } //end InsertOneRecord() 

        public int UpdateOneRecord(string query, object obj)
        {
                try
                {
                    //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                    Vehicle<int, string> upcastedTempVehicle = (Vehicle<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                    /*
                     * STEP 3: PERFORM INSERT INTO THE DATABASE
                     * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                     * - NEED TO OPEN CONNECTION TO THE DATABASE
                     * - NEED TO PERFORM THE INSERT
                     * - NEED TO CLOSE THE DATABASE
                     */

                    //STEP 4: INSTANTIATE A SQLCOMMAND
                    command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND FOR INSERTION

                    //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                    SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                    idParm.Value = upcastedTempVehicle.Id;

                    SqlParameter vinParm = new SqlParameter("@vinParm", SqlDbType.VarChar);
                    vinParm.Value = upcastedTempVehicle.Vin;

                    SqlParameter makeParm = new SqlParameter("@makeParm", SqlDbType.VarChar);
                    makeParm.Value = upcastedTempVehicle.Make;

                    SqlParameter modelParm = new SqlParameter("@modelParm", SqlDbType.VarChar);
                    modelParm.Value = upcastedTempVehicle.Model;

                    SqlParameter yearParm = new SqlParameter("@yearParm", SqlDbType.Int);
                    yearParm.Value = upcastedTempVehicle.Year;

                    SqlParameter colorParm = new SqlParameter("@colorParm", SqlDbType.VarChar);
                    colorParm.Value = upcastedTempVehicle.Color;

                    SqlParameter capacityParm = new SqlParameter("@capacityParm", SqlDbType.Int);
                    capacityParm.Value = upcastedTempVehicle.Capacity;

                    SqlParameter mileageParm = new SqlParameter("@mileageParm", SqlDbType.Int);
                    mileageParm.Value = upcastedTempVehicle.Mileage;

                    SqlParameter vehicleTypeParm = new SqlParameter("@vehicleTypeParm", SqlDbType.VarChar);
                    vehicleTypeParm.Value = upcastedTempVehicle.Type;

                    //ADD PARAMTERS TO THE SQLCOMMAND
                    command.Parameters.Add(idParm);
                    command.Parameters.Add(vinParm);
                    command.Parameters.Add(makeParm);
                    command.Parameters.Add(modelParm);
                    command.Parameters.Add(yearParm);
                    command.Parameters.Add(colorParm);
                    command.Parameters.Add(capacityParm);
                    command.Parameters.Add(mileageParm);
                    command.Parameters.Add(vehicleTypeParm);

                    //OPEN DATABASE CONNECTION
                    OpenConnection();

                    //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                    return command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    dacException.ErrorReturned = ex.Message;
                    return Convert.ToInt32(null);
                }
                finally
                {
                    //CLOSE DATABASE CONNECTION
                    //CloseConnection(); - Need not be done cos it is handled by command.Dispose. CloseConnection()-only with adapter object
                    command.Dispose();
                }
           
        }

        public int DeleteOneRecord(string query, object obj)
        {
            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Vehicle<int, string> upcastedTempVehicle = (Vehicle<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                /*
                 * STEP 3: PERFORM INSERT INTO THE DATABASE
                 * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                 * - NEED TO OPEN CONNECTION TO THE DATABASE
                 * - NEED TO PERFORM THE INSERT
                 * - NEED TO CLOSE THE DATABASE
                 */

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND FOR INSERTION

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempVehicle.Id;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                //CloseConnection(); - Need not be done cos it is handled by command.Dispose. CloseConnection()-only with adapter object
                command.Dispose();
            }
        } //end DeleteOneRecord()

    } //end class
} //end namespace
