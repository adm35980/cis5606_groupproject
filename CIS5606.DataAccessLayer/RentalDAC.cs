﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DomainLayer;

namespace CIS5606.DataAccessLayer
{
    public class RentalDAC : BaseAccessClass, IDataAcces
    {
        DACException dacException = new DACException();

        //INTERFACE METHODS -- ADD IMPLEMENTATION
        public int InsertOneRecord(string query, object obj)
        {
            try
            {
                //UPCASTING INCOMING OBJECT TO TYPE RENTAL INSTANCE
                Rental<int, string> upcastedTempRental = (Rental<int, string>)obj; //upcasting object to rental 

                command = new SqlCommand(query, this.EstablishConnection());

                SqlParameter rentalDateParm = new SqlParameter("@rentalDateParm", SqlDbType.VarChar);
                rentalDateParm.Value = upcastedTempRental.RentalDate;

                SqlParameter durationParm = new SqlParameter("@durationParm", SqlDbType.Int);
                durationParm.Value = upcastedTempRental.Duration;

                SqlParameter vehicleIdParm = new SqlParameter("@vehicleIdParm", SqlDbType.VarChar);
                vehicleIdParm.Value = upcastedTempRental.VehicleId;

                SqlParameter customerIDParm = new SqlParameter("@customerIDParm", SqlDbType.Int);
                customerIDParm.Value = upcastedTempRental.CustomerId;

                SqlParameter employeeIDParm = new SqlParameter("@employeeIDParm", SqlDbType.Int);
                employeeIDParm.Value = upcastedTempRental.EmployeeId;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(rentalDateParm);
                command.Parameters.Add(durationParm);
                command.Parameters.Add(vehicleIdParm);
                command.Parameters.Add(customerIDParm);
                command.Parameters.Add(employeeIDParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
        } //end InsertOneRecord() 

        public int UpdateOneRecord(string query, object obj)
        {
            try
            {
                //UPCASTING INCOMING OBJECT TO TYPE RENTAL INSTANCE
                Rental<int, string> upcastedTempRental = (Rental<int, string>)obj; //upcasting object to rental 

                command = new SqlCommand(query, this.EstablishConnection());

                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempRental.Id;

                SqlParameter rentalDateParm = new SqlParameter("@rentalDateParm", SqlDbType.VarChar);
                rentalDateParm.Value = upcastedTempRental.RentalDate;

                SqlParameter durationParm = new SqlParameter("@durationParm", SqlDbType.Int);
                durationParm.Value = upcastedTempRental.Duration;

                SqlParameter vehicleIdParm = new SqlParameter("@vehicleIdParm", SqlDbType.VarChar);
                vehicleIdParm.Value = upcastedTempRental.VehicleId;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);
                command.Parameters.Add(rentalDateParm);
                command.Parameters.Add(durationParm);
                command.Parameters.Add(vehicleIdParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                command.Dispose();
            }
        }

        public int DeleteOneRecord(string query, object obj)
        {
            try
            {
                //UPCASTING INCOMING OBJECT TO TYPE RENTAL INSTANCE
                Rental<int, string> upcastedTempRental = (Rental<int, string>)obj; //upcasting object to rental 

                command = new SqlCommand(query, this.EstablishConnection());

                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempRental.Id;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);
              

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                command.Dispose();
            }
        } //end DeleteOneRecord()
    }
}
