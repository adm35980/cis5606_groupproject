﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DomainLayer;

namespace CIS5606.DataAccessLayer
{
    public class CustomerDAC : BaseAccessClass, IDataAcces
    {
        DACException dacException = new DACException();

        //INTERFACE METHODS -- ADD IMPLEMENTATION
        public int InsertOneRecord(string query, object obj)
        {
            /*
             * ACTION TAKEN: 
             * - WILL RECEIVE AN OBJECT AND QUERY FROM INTERMEDIATE LAYER [BUSINESS LAYER]
             * - UPCAST THE PASSED IN OBJECT TO TYPE 'VEHICLE'
             * - WILL PERFORM THE INSERT INTO THE DATABASE
             * - WILL USE A SQLCOMMAND INSTEAD OF A DATA ADAPTER
             */

            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Customer<int, string> upcastedTempCustomer = (Customer<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                /*
                 * STEP 3: PERFORM INSERT INTO THE DATABASE
                 * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                 * - NEED TO OPEN CONNECTION TO THE DATABASE
                 * - NEED TO PERFORM THE INSERT
                 * - NEED TO CLOSE THE DATABASE
                 */

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND FOR INSERTION

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter firstNameParm = new SqlParameter("@firstNameParm", SqlDbType.VarChar);
                firstNameParm.Value = upcastedTempCustomer.FirstName;

                SqlParameter lastNameParm = new SqlParameter("@lastNameParm", SqlDbType.VarChar);
                lastNameParm.Value = upcastedTempCustomer.LastName;

                SqlParameter phoneParm = new SqlParameter("@phoneParm", SqlDbType.VarChar);
                phoneParm.Value = upcastedTempCustomer.Phone;

                SqlParameter emailParm = new SqlParameter("@emailParm", SqlDbType.VarChar);
                emailParm.Value = upcastedTempCustomer.Email;

                SqlParameter addressParm = new SqlParameter("@addressParm", SqlDbType.VarChar);
                addressParm.Value = upcastedTempCustomer.Address;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(firstNameParm);
                command.Parameters.Add(lastNameParm);
                command.Parameters.Add(phoneParm);
                command.Parameters.Add(emailParm);
                command.Parameters.Add(addressParm);
               
                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                //CloseConnection(); - Need not be done cos it is handled by command.Dispose. CloseConnection()-only with adapter object
                command.Dispose();
            }
            
        } //end InsertOneRecord() 

        public int UpdateOneRecord(string query, object obj)
        {

            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Customer<int, string> upcastedTempCustomer = (Customer<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempCustomer.Id;

                SqlParameter firstNameParam = new SqlParameter("@firstNameParm", SqlDbType.VarChar);
                firstNameParam.Value = upcastedTempCustomer.FirstName;

                SqlParameter lastNameParam = new SqlParameter("@lastNameParm", SqlDbType.VarChar);
                lastNameParam.Value = upcastedTempCustomer.LastName;

                SqlParameter phoneParam = new SqlParameter("@phoneParm", SqlDbType.VarChar);
                phoneParam.Value = upcastedTempCustomer.Phone;

                SqlParameter emailParam = new SqlParameter("@emailParm", SqlDbType.VarChar);
                emailParam.Value = upcastedTempCustomer.Email;

                SqlParameter addressParam = new SqlParameter("@addressParm", SqlDbType.VarChar);
                addressParam.Value = upcastedTempCustomer.Address;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);
                command.Parameters.Add(firstNameParam);
                command.Parameters.Add(lastNameParam);
                command.Parameters.Add(phoneParam);
                command.Parameters.Add(emailParam);
                command.Parameters.Add(addressParam);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                //CloseConnection(); - Need not be done cos it is handled by command.Dispose. CloseConnection()-only with adapter object
                command.Dispose();
            }
        }

        public int DeleteOneRecord(string query, object obj)
        {

            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Customer<int, string> upcastedTempCustomer = (Customer<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempCustomer.Id;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                //CloseConnection(); - Need not be done cos it is handled by command.Dispose. CloseConnection()-only with adapter object
                command.Dispose();
            }
        } //end DeleteOneRecord()
       
    } //end class
} //end namespace
