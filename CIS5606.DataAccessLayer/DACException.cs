﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DataAccessLayer
{
    public class DACException : Exception
    {
        public String ErrorReturned { get; set; }

    }
}
