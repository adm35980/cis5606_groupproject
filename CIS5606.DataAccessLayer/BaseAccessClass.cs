﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DataAccessLayer
{
    public class BaseAccessClass
    {
        //DATABASE VARIABLES
        //VARIABLE FOR CONNECTION -- NEED TO CONNECT TO DATABASE
        protected SqlConnection sqlconnection;
        //CONNECTION STRING -- SPECIFIES DATABASE NAME AND LOCATION
        protected String connString = ConfigurationManager.ConnectionStrings
            ["CIS5606_GroupProject.Properties.Settings.DatabaseConnection"].ConnectionString;
        //DATA ADAPTER -- CARRIES QUERIES TO THE DATABASE
        protected SqlDataAdapter adapter;
        //SQL COMMAND -- USE FOR INSERTION AND DELETION
        protected SqlCommand command;
        //DATA READER -- CARRIES QUERIES TO THE DATABASE IN ABSENCE OF A DATA ADAPTER
        protected SqlDataReader rdr;


        //ERROR EXCEPTION INSTANCE
        DACException dacException = new DACException();



        //BASIC CONNECTION METHODS
        public SqlConnection EstablishConnection() //SETUP CONNECTION TO DATABASE
        {
            try
            {
                //CREATE CONNECTION IF IT DOESN'T EXIST ALREADY
                if (sqlconnection == null)
                {
                    sqlconnection = new SqlConnection(connString);
                } //end if statement

                return sqlconnection;
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return null;
            }
        } //end GetConnection()

        public void OpenConnection() //OPEN THE DATABASE CONNECTION
        {
            try
            {
                if (sqlconnection.State == System.Data.ConnectionState.Closed) //IF CONNECTION IS CLOSED, OPEN THE CONNECTION
                {
                    sqlconnection.Open();
                } //end if statement
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
            }

        } //end OpenConnection()

        public void CloseConnection()
        {
            try
            {
                if (sqlconnection.State == System.Data.ConnectionState.Open) //IF CONNECTION IS OPEN, CLOSE THE CONNECTION
                {
                    sqlconnection.Close();
                } //end if statement
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
            }
        } //end CloseConnection()

        public DataSet RetrieveData(string query) //RETRIEVE ALL DATA FROM THE DATABASE -- NEED SQL QUERY AS ARGUMENT
        {
            //VARIABLE FOR RETURNED DATA
            DataSet dataSet = new DataSet();

            try
            {
                adapter = new SqlDataAdapter(query, EstablishConnection());
                OpenConnection();
                adapter.Fill(dataSet);
                CloseConnection();
                return dataSet;
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return null;
            }
        }//end retrieve data

        //TO INSERT/DELETE RECORDS OF DATA

    } //end class
} //end namespace
