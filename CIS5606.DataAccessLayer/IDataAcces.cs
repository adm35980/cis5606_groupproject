﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS5606.DataAccessLayer
{
    interface IDataAcces
    {
        //INTERFACE METHODS
        int InsertOneRecord(string query, object obj);

        int UpdateOneRecord(string query, object obj);

        int DeleteOneRecord(string queyr, object obj);
    }
}
