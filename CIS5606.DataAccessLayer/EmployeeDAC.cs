﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIS5606.DomainLayer;

namespace CIS5606.DataAccessLayer
{
    public class EmployeeDAC : BaseAccessClass, IDataAcces
    {
        DACException dacException = new DACException();

        //INTERFACE METHODS -- ADD IMPLEMENTATION
        public int InsertOneRecord(string query, object obj)
        {
            /*
            * ACTION TAKEN: 
            * - WILL RECEIVE AN OBJECT AND QUERY FROM INTERMEDIATE LAYER [BUSINESS LAYER]
            * - UPCAST THE PASSED IN OBJECT TO TYPE 'VEHICLE'
            * - WILL PERFORM THE INSERT INTO THE DATABASE
            * - WILL USE A SQLCOMMAND INSTEAD OF A DATA ADAPTER
            */

            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Employee<int, string> upcastedTempEmployee = (Employee<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                /*
                 * STEP 3: PERFORM INSERT INTO THE DATABASE
                 * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                 * - NEED TO OPEN CONNECTION TO THE DATABASE
                 * - NEED TO PERFORM THE INSERT
                 * - NEED TO CLOSE THE DATABASE
                 */

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND FOR INSERTION

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter firstNameParm = new SqlParameter("@firstNameParm", SqlDbType.VarChar);
                firstNameParm.Value = upcastedTempEmployee.FirstName;

                SqlParameter lastNameParm = new SqlParameter("@lastNameParm", SqlDbType.VarChar);
                lastNameParm.Value = upcastedTempEmployee.LastName;

                SqlParameter phoneParm = new SqlParameter("@phoneParm", SqlDbType.VarChar);
                phoneParm.Value = upcastedTempEmployee.Phone;

                SqlParameter emailParm = new SqlParameter("@emailParm", SqlDbType.VarChar);
                emailParm.Value = upcastedTempEmployee.Email;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(firstNameParm);
                command.Parameters.Add(lastNameParm);
                command.Parameters.Add(phoneParm);
                command.Parameters.Add(emailParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                command.Dispose();
            }
        } //end InsertOneRecord() 

        public int UpdateOneRecord(string query, object obj)
        {
            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Employee<int, string> upcastedTempEmployee = (Employee<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                /*
                 * STEP 3: PERFORM INSERT INTO THE DATABASE
                 * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                 * - NEED TO OPEN CONNECTION TO THE DATABASE
                 * - NEED TO PERFORM THE INSERT
                 * - NEED TO CLOSE THE DATABASE
                 */

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempEmployee.Id;

                SqlParameter firstNameParm = new SqlParameter("@firstNameParm", SqlDbType.VarChar);
                firstNameParm.Value = upcastedTempEmployee.FirstName;

                SqlParameter lastNameParm = new SqlParameter("@lastNameParm", SqlDbType.VarChar);
                lastNameParm.Value = upcastedTempEmployee.LastName;

                SqlParameter phoneParm = new SqlParameter("@phoneParm", SqlDbType.VarChar);
                phoneParm.Value = upcastedTempEmployee.Phone;

                SqlParameter emailParm = new SqlParameter("@emailParm", SqlDbType.VarChar);
                emailParm.Value = upcastedTempEmployee.Email;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);
                command.Parameters.Add(firstNameParm);
                command.Parameters.Add(lastNameParm);
                command.Parameters.Add(phoneParm);
                command.Parameters.Add(emailParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                command.Dispose();
            }
        }

        public int DeleteOneRecord(string query, object obj)
        {
            try
            {
                //STEP 2: UPCAST INCOMING OBJECT TO TYPE VEHICLE
                Employee<int, string> upcastedTempEmployee = (Employee<int, string>)obj; //UPCASTED INCOMING OBJECT (OBJ) TO TYPE VEHICLE

                /*
                 * STEP 3: PERFORM INSERT INTO THE DATABASE
                 * - NEED TO ESTABLISH CONNECITON WITH THE DATABASE
                 * - NEED TO OPEN CONNECTION TO THE DATABASE
                 * - NEED TO PERFORM THE INSERT
                 * - NEED TO CLOSE THE DATABASE
                 */

                //STEP 4: INSTANTIATE A SQLCOMMAND
                command = new SqlCommand(query, EstablishConnection()); //WILL ESTABLISH CONNECTION TO DATABASE AND CREATE NEW SQLCOMMAND

                //SQL PARAMETERS -- TELLS THE DATABASE THE PASSED IN USER VALUES ARE JUST DATA AND NOT QUERIES
                SqlParameter idParm = new SqlParameter("@idParm", SqlDbType.Int);
                idParm.Value = upcastedTempEmployee.Id;

                //ADD PARAMTERS TO THE SQLCOMMAND
                command.Parameters.Add(idParm);

                //OPEN DATABASE CONNECTION
                OpenConnection();

                //ExecuteNonQuery() returns the # of rows affected by the command -- returns as type [int]
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dacException.ErrorReturned = ex.Message;
                return Convert.ToInt32(null);
            }
            finally
            {
                //CLOSE DATABASE CONNECTION
                command.Dispose();
            }
        } //end DeleteOneRecord()

    } //end class
} //end namespace
