﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS5606_GroupProject
{
    public partial class HomePage : Form
    {
        private static HomePage homePage;

        private HomePage()
        {
            InitializeComponent();
        }

        internal static HomePage GetHomePageInstance()
        {
            if(homePage == null)
            {
                homePage = new HomePage();
            }

            return homePage;
        }

        private void HomePage_FormClosing(object sender, FormClosedEventArgs e)
        {
            homePage = null;
        }

    }
}
