﻿
namespace CIS5606_GroupProject
{
    partial class CreateRental
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rentalDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.durationComboBox = new System.Windows.Forms.ComboBox();
            this.rentalExitButton = new System.Windows.Forms.Button();
            this.rentalSubmitButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.employeeIdComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.customerIdComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.customerLastNameLabel = new System.Windows.Forms.Label();
            this.customerFirstNameLabel = new System.Windows.Forms.Label();
            this.employeeLastNameLabel = new System.Windows.Forms.Label();
            this.employeeFirstNameLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            this.vehicleIdComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.makeLabel = new System.Windows.Forms.Label();
            this.modelLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.capacityLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.yearLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.vehicleVINLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Goudy Stout", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(112, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(790, 82);
            this.label1.TabIndex = 1;
            this.label1.Text = "APB RENTALS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(385, 321);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Vehicle VIN:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(184, 561);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Rental Start Date:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(579, 566);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Duration:";
            // 
            // rentalDateTimePicker
            // 
            this.rentalDateTimePicker.Location = new System.Drawing.Point(330, 561);
            this.rentalDateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.rentalDateTimePicker.Name = "rentalDateTimePicker";
            this.rentalDateTimePicker.Size = new System.Drawing.Size(232, 22);
            this.rentalDateTimePicker.TabIndex = 8;
            // 
            // durationComboBox
            // 
            this.durationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.durationComboBox.FormattingEnabled = true;
            this.durationComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.durationComboBox.Location = new System.Drawing.Point(683, 561);
            this.durationComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.durationComboBox.Name = "durationComboBox";
            this.durationComboBox.Size = new System.Drawing.Size(174, 24);
            this.durationComboBox.TabIndex = 9;
            // 
            // rentalExitButton
            // 
            this.rentalExitButton.Location = new System.Drawing.Point(602, 700);
            this.rentalExitButton.Margin = new System.Windows.Forms.Padding(4);
            this.rentalExitButton.Name = "rentalExitButton";
            this.rentalExitButton.Size = new System.Drawing.Size(100, 28);
            this.rentalExitButton.TabIndex = 10;
            this.rentalExitButton.Text = "&Exit";
            this.rentalExitButton.UseVisualStyleBackColor = true;
            this.rentalExitButton.Click += new System.EventHandler(this.rentalExitButton_Click);
            // 
            // rentalSubmitButton
            // 
            this.rentalSubmitButton.Location = new System.Drawing.Point(327, 700);
            this.rentalSubmitButton.Margin = new System.Windows.Forms.Padding(4);
            this.rentalSubmitButton.Name = "rentalSubmitButton";
            this.rentalSubmitButton.Size = new System.Drawing.Size(100, 28);
            this.rentalSubmitButton.TabIndex = 11;
            this.rentalSubmitButton.Text = "&Submit";
            this.rentalSubmitButton.UseVisualStyleBackColor = true;
            this.rentalSubmitButton.Click += new System.EventHandler(this.rentalSubmitButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Employee:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(359, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "First Name:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(549, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "Last Name:";
            // 
            // employeeIdComboBox
            // 
            this.employeeIdComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.employeeIdComboBox.FormattingEnabled = true;
            this.employeeIdComboBox.Location = new System.Drawing.Point(156, 32);
            this.employeeIdComboBox.Name = "employeeIdComboBox";
            this.employeeIdComboBox.Size = new System.Drawing.Size(143, 24);
            this.employeeIdComboBox.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(44, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 17);
            this.label10.TabIndex = 21;
            this.label10.Text = "Customer:";
            // 
            // customerIdComboBox
            // 
            this.customerIdComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customerIdComboBox.FormattingEnabled = true;
            this.customerIdComboBox.Location = new System.Drawing.Point(156, 81);
            this.customerIdComboBox.Name = "customerIdComboBox";
            this.customerIdComboBox.Size = new System.Drawing.Size(143, 24);
            this.customerIdComboBox.TabIndex = 22;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.customerLastNameLabel);
            this.groupBox1.Controls.Add(this.customerFirstNameLabel);
            this.groupBox1.Controls.Add(this.employeeLastNameLabel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.employeeFirstNameLabel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.employeeIdComboBox);
            this.groupBox1.Controls.Add(this.customerIdComboBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(157, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 123);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Information";
            // 
            // customerLastNameLabel
            // 
            this.customerLastNameLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customerLastNameLabel.Location = new System.Drawing.Point(511, 75);
            this.customerLastNameLabel.Name = "customerLastNameLabel";
            this.customerLastNameLabel.Size = new System.Drawing.Size(156, 30);
            this.customerLastNameLabel.TabIndex = 26;
            this.customerLastNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // customerFirstNameLabel
            // 
            this.customerFirstNameLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customerFirstNameLabel.Location = new System.Drawing.Point(327, 75);
            this.customerFirstNameLabel.Name = "customerFirstNameLabel";
            this.customerFirstNameLabel.Size = new System.Drawing.Size(156, 30);
            this.customerFirstNameLabel.TabIndex = 25;
            this.customerFirstNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // employeeLastNameLabel
            // 
            this.employeeLastNameLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.employeeLastNameLabel.Location = new System.Drawing.Point(511, 31);
            this.employeeLastNameLabel.Name = "employeeLastNameLabel";
            this.employeeLastNameLabel.Size = new System.Drawing.Size(156, 30);
            this.employeeLastNameLabel.TabIndex = 24;
            this.employeeLastNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // employeeFirstNameLabel
            // 
            this.employeeFirstNameLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.employeeFirstNameLabel.Location = new System.Drawing.Point(327, 32);
            this.employeeFirstNameLabel.Name = "employeeFirstNameLabel";
            this.employeeFirstNameLabel.Size = new System.Drawing.Size(156, 30);
            this.employeeFirstNameLabel.TabIndex = 23;
            this.employeeFirstNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(438, 246);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 17);
            this.label11.TabIndex = 26;
            this.label11.Text = "Vehicle Information";
            // 
            // messageLabel
            // 
            this.messageLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.messageLabel.Location = new System.Drawing.Point(83, 606);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(844, 73);
            this.messageLabel.TabIndex = 28;
            // 
            // vehicleIdComboBox
            // 
            this.vehicleIdComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.vehicleIdComboBox.FormattingEnabled = true;
            this.vehicleIdComboBox.Location = new System.Drawing.Point(484, 278);
            this.vehicleIdComboBox.Name = "vehicleIdComboBox";
            this.vehicleIdComboBox.Size = new System.Drawing.Size(158, 24);
            this.vehicleIdComboBox.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(385, 355);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 17);
            this.label12.TabIndex = 30;
            this.label12.Text = "Make:";
            // 
            // makeLabel
            // 
            this.makeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.makeLabel.Location = new System.Drawing.Point(484, 349);
            this.makeLabel.Name = "makeLabel";
            this.makeLabel.Size = new System.Drawing.Size(158, 23);
            this.makeLabel.TabIndex = 31;
            // 
            // modelLabel
            // 
            this.modelLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modelLabel.Location = new System.Drawing.Point(484, 389);
            this.modelLabel.Name = "modelLabel";
            this.modelLabel.Size = new System.Drawing.Size(158, 23);
            this.modelLabel.TabIndex = 33;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(385, 395);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 17);
            this.label14.TabIndex = 32;
            this.label14.Text = "Model:";
            // 
            // capacityLabel
            // 
            this.capacityLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.capacityLabel.Location = new System.Drawing.Point(484, 468);
            this.capacityLabel.Name = "capacityLabel";
            this.capacityLabel.Size = new System.Drawing.Size(158, 23);
            this.capacityLabel.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(385, 474);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 17);
            this.label16.TabIndex = 34;
            this.label16.Text = "Capacity:";
            // 
            // yearLabel
            // 
            this.yearLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.yearLabel.Location = new System.Drawing.Point(484, 429);
            this.yearLabel.Name = "yearLabel";
            this.yearLabel.Size = new System.Drawing.Size(158, 23);
            this.yearLabel.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(385, 435);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 17);
            this.label18.TabIndex = 36;
            this.label18.Text = "Year:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(429, 519);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(156, 17);
            this.label20.TabIndex = 38;
            this.label20.Text = "Duration Information";
            // 
            // vehicleVINLabel
            // 
            this.vehicleVINLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vehicleVINLabel.Location = new System.Drawing.Point(484, 315);
            this.vehicleVINLabel.Name = "vehicleVINLabel";
            this.vehicleVINLabel.Size = new System.Drawing.Size(158, 23);
            this.vehicleVINLabel.TabIndex = 40;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(385, 285);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 17);
            this.label13.TabIndex = 39;
            this.label13.Text = "ID:";
            // 
            // CreateRental
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 791);
            this.Controls.Add(this.vehicleVINLabel);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.yearLabel);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.capacityLabel);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.modelLabel);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.makeLabel);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.vehicleIdComboBox);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rentalSubmitButton);
            this.Controls.Add(this.rentalExitButton);
            this.Controls.Add(this.durationComboBox);
            this.Controls.Add(this.rentalDateTimePicker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CreateRental";
            this.Text = "Rental_Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Rental_Form_FormClosing);
            this.Load += new System.EventHandler(this.Rental_Form_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker rentalDateTimePicker;
        private System.Windows.Forms.ComboBox durationComboBox;
        private System.Windows.Forms.Button rentalExitButton;
        private System.Windows.Forms.Button rentalSubmitButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox employeeIdComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox customerIdComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.ComboBox vehicleIdComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label makeLabel;
        private System.Windows.Forms.Label modelLabel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label capacityLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label yearLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label customerLastNameLabel;
        private System.Windows.Forms.Label customerFirstNameLabel;
        private System.Windows.Forms.Label employeeLastNameLabel;
        private System.Windows.Forms.Label employeeFirstNameLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label vehicleVINLabel;
        private System.Windows.Forms.Label label13;
    }
}