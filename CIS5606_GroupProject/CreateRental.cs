﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class CreateRental : Form
    {
        private static CreateRental rentalForm;

        //private ctor
        private CreateRental()
        {
            InitializeComponent();
        }

        private void Rental_Form_Load(object sender, EventArgs e)
        {
            //INTERMEDIATE LAYER
            EmployeeBL employeeBL = new EmployeeBL();
            CustomerBL customerBL = new CustomerBL();
            VehicleBL vehicleBL = new VehicleBL();

            //GET THE DATA
            DataTable employeeInfo = employeeBL.FetchData();
            DataTable customerInfo = customerBL.FetchData();
            DataTable vehicleInfo = vehicleBL.FetchData();

            //INSTANTIATE BINDING SOURCE
            BindingSource employeeBindingSource = new BindingSource();
            employeeBindingSource.DataSource = employeeInfo;

            
            BindingSource customerBindingSource = new BindingSource();
            customerBindingSource.DataSource = customerInfo;
            
            
            BindingSource vehicleBindingSource = new BindingSource();
            vehicleBindingSource.DataSource = vehicleInfo;
            



            //DATA BINDING CONTROLS
            //COMBOBOX:
            employeeIdComboBox.DataSource = employeeBindingSource;
            employeeIdComboBox.ValueMember = "Id";
            employeeIdComboBox.DisplayMember = "Id";
            employeeIdComboBox.DataBindings.Add("text", employeeBindingSource, "Id", false, DataSourceUpdateMode.Never);

            
            customerIdComboBox.DataSource = customerBindingSource;
            customerIdComboBox.ValueMember = "Id";
            customerIdComboBox.DisplayMember = Convert.ToString("Id");
            customerIdComboBox.DataBindings.Add("text", customerBindingSource, "Id", false, DataSourceUpdateMode.Never);

            
            vehicleIdComboBox.DataSource = vehicleBindingSource;
            vehicleIdComboBox.ValueMember = "Id";
            vehicleIdComboBox.DisplayMember = Convert.ToString("Id");
            vehicleIdComboBox.DataBindings.Add("text", vehicleBindingSource, "Id", false, DataSourceUpdateMode.Never);
            

            //LABELS:
            employeeFirstNameLabel.DataBindings.Add("text", employeeBindingSource, "First_Name", false);
            employeeLastNameLabel.DataBindings.Add("text", employeeBindingSource, "Last_Name", false);
            
            
            customerFirstNameLabel.DataBindings.Add("text", customerBindingSource, "First_Name", false);
            customerLastNameLabel.DataBindings.Add("text", customerBindingSource, "Last_Name", false);

            vehicleVINLabel.DataBindings.Add("text", vehicleBindingSource, "VIN", false);
            makeLabel.DataBindings.Add("text", vehicleBindingSource, "Make", false);
            modelLabel.DataBindings.Add("text", vehicleBindingSource, "Model", false);
            yearLabel.DataBindings.Add("text", vehicleBindingSource, "Year", false);
            capacityLabel.DataBindings.Add("text", vehicleBindingSource, "Capacity", false);

        }

        //creating a singleton instance 
        //method returns instance of form if null
        internal static CreateRental GetRentalInstance()
        {
            if(rentalForm==null)
            {
                rentalForm = new CreateRental();
            }
            return rentalForm;
        }

        private void rentalExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Rental_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            //remove any existing instances.(making all existing instances null)
            rentalForm = null;
        }

        private void rentalSubmitButton_Click(object sender, EventArgs e)
        {
            /*
            * PERFORMING AN INSERT HERE
            * - NEED TO ACCESS THE DATA ACCESS LAYER THROUGH THE BUSINESS LAYER
            * - NEED TO CREATE AN INSTANCE OF A RENTAL FROM THE DOMAIN LAYER CLASS
            * - PASS THAT RENTAL INSTANCE TO THE BUSINESS LAYER, WHICH PASSES IT TO THE DATA ACCESS LAYER AND PERFORMS THE INSERT
            * - DISPLAY SUCCESSFUL INSERTION MESSAGE IN LABEL ON FORM
            * - FINALLY, CHECK DATABASE TO SEE IF ENTRY WAS SUCCESSFUL
           */

            //ERROR EXCEPTION INSTANCIATION
            BLException blException = new BLException();

            //STEP 1: ACCESS INTERMEDIATE LAYER INSTANTIATE CLASS
            RentalBL rentalBL = new RentalBL();

            messageLabel.Text = "";

            if(customerIdComboBox.SelectedIndex!=-1 && employeeIdComboBox.SelectedIndex!=-1) //IF CUSTOMER AND EMPLOYEE SELECTED VIA DROPDOWN
            {
                if(vehicleIdComboBox.SelectedIndex!=-1 && durationComboBox.SelectedIndex!=-1) //IF VEHICLE AND DURATION SELECTED VIA DROPDOWN
                {
                    //STEP 2: CREATE VEHICLE DOMAIN CLASS INSTANCE -- HAVE TO UPCASE MOST COMBOXES TO DATAROWVIEW BC THE INFO FOR THEM ARE BOUNDED FROM A DATATABLE [IN THE FORM_LOAD CODE]

                    Rental<int, string> tempRental = new Rental<int, string>(-1, Convert.ToString(rentalDateTimePicker.Value), Convert.ToInt32(durationComboBox.SelectedItem),
                                                                                                    Convert.ToInt32(vehicleIdComboBox.SelectedValue),
                                                                                                    Convert.ToInt32(customerIdComboBox.SelectedValue),
                                                                                                    Convert.ToInt32(employeeIdComboBox.SelectedValue));
                    try
                    {
                        //STEP 3: PASS THAT VEHICLE INSTANCE TO THE BUSINESS LAYER
                        rentalBL.InsertNewRecord(tempRental);

                        //STEP 4: DISPLAY MESSAGE ON FORM
                        messageLabel.Text += "You have successfully created a new rental!!!";
                    }
                    catch (Exception ex)
                    {
                        blException.ErrorReturned = ex.Message;
                    }

                    //Action delegate to track duration 
                    Action<int> selectedDuration = (incomingNumber) =>
                    {
                        if (incomingNumber > 2) //Duration > 2days
                            messageLabel.Text += $"Customer is eligible for 15% discount!";
                        else
                            messageLabel.Text += $"Customer is not eligible for any discount";
                    };

                    selectedDuration(int.Parse(durationComboBox.SelectedItem.ToString()));

                }
                else
                {
                    messageLabel.Text = "Please select VIN from drop down list and provide duration of rent!";
                }
            }
            else
            {
                messageLabel.Text = "Please select employeeId/CustomerId from the drop-down list!";
            }

            
        }
    }
}
