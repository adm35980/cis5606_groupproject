﻿
namespace CIS5606_GroupProject
{
    partial class CreateCustomerAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.fNameTextBox = new System.Windows.Forms.TextBox();
            this.lNameTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.eMailTextBox = new System.Windows.Forms.TextBox();
            this.homeAddresstextBox = new System.Windows.Forms.TextBox();
            this.accountSubmitButton = new System.Windows.Forms.Button();
            this.accountExitButton = new System.Windows.Forms.Button();
            this.messageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Goudy Stout", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(776, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer Account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "First Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(110, 170);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Last Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(110, 216);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Phone Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 265);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Email Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(110, 307);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Home Address";
            // 
            // fNameTextBox
            // 
            this.fNameTextBox.Location = new System.Drawing.Point(243, 116);
            this.fNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fNameTextBox.Name = "fNameTextBox";
            this.fNameTextBox.Size = new System.Drawing.Size(356, 22);
            this.fNameTextBox.TabIndex = 8;
            // 
            // lNameTextBox
            // 
            this.lNameTextBox.Location = new System.Drawing.Point(243, 165);
            this.lNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.lNameTextBox.Name = "lNameTextBox";
            this.lNameTextBox.Size = new System.Drawing.Size(356, 22);
            this.lNameTextBox.TabIndex = 9;
            // 
            // phoneNumberTextBox
            // 
            this.phoneNumberTextBox.Location = new System.Drawing.Point(243, 216);
            this.phoneNumberTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.phoneNumberTextBox.Name = "phoneNumberTextBox";
            this.phoneNumberTextBox.Size = new System.Drawing.Size(356, 22);
            this.phoneNumberTextBox.TabIndex = 10;
            // 
            // eMailTextBox
            // 
            this.eMailTextBox.Location = new System.Drawing.Point(243, 260);
            this.eMailTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.eMailTextBox.Name = "eMailTextBox";
            this.eMailTextBox.Size = new System.Drawing.Size(356, 22);
            this.eMailTextBox.TabIndex = 11;
            // 
            // homeAddresstextBox
            // 
            this.homeAddresstextBox.Location = new System.Drawing.Point(243, 299);
            this.homeAddresstextBox.Margin = new System.Windows.Forms.Padding(4);
            this.homeAddresstextBox.Multiline = true;
            this.homeAddresstextBox.Name = "homeAddresstextBox";
            this.homeAddresstextBox.Size = new System.Drawing.Size(356, 25);
            this.homeAddresstextBox.TabIndex = 13;
            // 
            // accountSubmitButton
            // 
            this.accountSubmitButton.Location = new System.Drawing.Point(186, 405);
            this.accountSubmitButton.Margin = new System.Windows.Forms.Padding(4);
            this.accountSubmitButton.Name = "accountSubmitButton";
            this.accountSubmitButton.Size = new System.Drawing.Size(100, 28);
            this.accountSubmitButton.TabIndex = 14;
            this.accountSubmitButton.Text = "&Submit";
            this.accountSubmitButton.UseVisualStyleBackColor = true;
            this.accountSubmitButton.Click += new System.EventHandler(this.accountSubmitButton_Click);
            // 
            // accountExitButton
            // 
            this.accountExitButton.Location = new System.Drawing.Point(459, 405);
            this.accountExitButton.Margin = new System.Windows.Forms.Padding(4);
            this.accountExitButton.Name = "accountExitButton";
            this.accountExitButton.Size = new System.Drawing.Size(100, 28);
            this.accountExitButton.TabIndex = 15;
            this.accountExitButton.Text = "&Exit";
            this.accountExitButton.UseVisualStyleBackColor = true;
            this.accountExitButton.Click += new System.EventHandler(this.accountExitButton_Click);
            // 
            // messageLabel
            // 
            this.messageLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.messageLabel.Location = new System.Drawing.Point(111, 344);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(566, 45);
            this.messageLabel.TabIndex = 16;
            this.messageLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // CreateCustomerAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 446);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.accountExitButton);
            this.Controls.Add(this.accountSubmitButton);
            this.Controls.Add(this.homeAddresstextBox);
            this.Controls.Add(this.eMailTextBox);
            this.Controls.Add(this.phoneNumberTextBox);
            this.Controls.Add(this.lNameTextBox);
            this.Controls.Add(this.fNameTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CreateCustomerAccount";
            this.Text = "Create_Account";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateCustomerAccount_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox fNameTextBox;
        private System.Windows.Forms.TextBox lNameTextBox;
        private System.Windows.Forms.TextBox phoneNumberTextBox;
        private System.Windows.Forms.TextBox eMailTextBox;
        private System.Windows.Forms.TextBox homeAddresstextBox;
        private System.Windows.Forms.Button accountSubmitButton;
        private System.Windows.Forms.Button accountExitButton;
        private System.Windows.Forms.Label messageLabel;
    }
}