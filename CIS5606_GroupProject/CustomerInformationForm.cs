﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class CustomerInformationForm : Form
    {
        //SINGLETON
        private static CustomerInformationForm customerInformationForm;

        internal static CustomerInformationForm GetCustomerInstance()
        {
            if(customerInformationForm == null)
            {
                customerInformationForm = new CustomerInformationForm();
            }
            return customerInformationForm;
        }
        //FORM LOAD
        private CustomerInformationForm()
        {
            InitializeComponent();
        }

        private void CustomerInformationForm_Load(object sender, EventArgs e)
        {
            PopulateGridView();

            //GRIDVIEW BUTTONS
            //delete button
            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            deleteButton.Name = "deleteButton";
            deleteButton.HeaderText = ""; //nothing in delete column header
            deleteButton.HeaderText = null;
            deleteButton.Text = "Delete"; //gets and set button text
            deleteButton.UseColumnTextForButtonValue = true; //display text on button
            customerDataGridView.Columns.Add(deleteButton); //add button to gridview

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            editButton.Name = "editButton";
            editButton.HeaderText = ""; //nothing in delete column header
            editButton.HeaderText = null;
            editButton.Text = "Edit"; //gets and set button text
            editButton.UseColumnTextForButtonValue = true; //display text on button
            customerDataGridView.Columns.Add(editButton); //add button to gridview

        }

        public void PopulateGridView()
        {
            CustomerBL customerBL = new CustomerBL();
            customerDataGridView.DataSource = customerBL.FetchData();
        }

        
        //DELETING & UPDATING
        private void customerDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == customerDataGridView.Columns["deleteButton"].Index)
            {
                int id = Convert.ToInt32(customerDataGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the VIN column of the row being deleted

                Customer<int, string> tempCustomer = new Customer<int, string>(id, "%", "%", "%", "%", "%"); //create temporary vehicle instance

                CustomerBL customerBL = new CustomerBL(); //pass temporary vehicle instance to intermediate layer for deletion

                int recordsDeleted = customerBL.DeleteOneRecord(tempCustomer);

                if(recordsDeleted < 1)
                {
                    messageLabel.Text = "Cusomter Account currently in use in an open rental. Please delete or update all rentals using customer account.";
                }
                else
                {
                    messageLabel.Text = $"You have deleted {recordsDeleted} record(s)!!!";
                }


                PopulateGridView();
            }
            if (e.ColumnIndex == customerDataGridView.Columns["editButton"].Index)
            {
                int id = Convert.ToInt32(customerDataGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the Id column 
                string fname = customerDataGridView.Rows[e.RowIndex].Cells["First_Name"].Value.ToString(); //gets the new value in the VIN column
                string lname = customerDataGridView.Rows[e.RowIndex].Cells["Last_Name"].Value.ToString(); //gets the value in the make column
                string phone = customerDataGridView.Rows[e.RowIndex].Cells["Phone"].Value.ToString(); //gets the value in the model column
                string email = customerDataGridView.Rows[e.RowIndex].Cells["Email"].Value.ToString(); //gets the value in the model column
                string address = customerDataGridView.Rows[e.RowIndex].Cells["Address"].Value.ToString(); //gets the value in the model column

                idLabel.Text = id.ToString();
                fnameTextBox.Text = fname;
                lnameTextBox.Text = lname;
                phoneTextBox.Text = phone;
                emailTextBox.Text = email;
                addressTextBox.Text = address;

            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            Customer<int, string> temp = new Customer<int, string>(Convert.ToInt32(idLabel.Text), fnameTextBox.Text, lnameTextBox.Text, phoneTextBox.Text, emailTextBox.Text, addressTextBox.Text);

            CustomerBL customerBL = new CustomerBL();
            int records = customerBL.UpdateOneRecord(temp);

            messageLabel.Text = $"You have successfully updated customer account #{temp.Id}!!!";
            messageLabel.ForeColor = System.Drawing.Color.Green;

            PopulateGridView();

            idLabel.Text = string.Empty;
            fnameTextBox.Text = string.Empty;
            lnameTextBox.Text = string.Empty;
            phoneTextBox.Text = string.Empty;
            emailTextBox.Text = string.Empty;
            addressTextBox.Text = string.Empty;

        }

        private void CustomerInformationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            customerInformationForm = null;
        }

    }
}
