﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class CreateEmployee : Form
    {
        //SINGLETONT ITEMS
        private static CreateEmployee employeeInstance;
        //private ctor
        private CreateEmployee()
        {
            InitializeComponent();
        }

        internal static CreateEmployee CreateEmployeeInstance()
        {
            if(employeeInstance==null)
            {
                employeeInstance = new CreateEmployee();
            }
            return employeeInstance;
        }

        //FORM FUNCTINONALITY ITEMS
        private void submitButton_Click(object sender, EventArgs e)
        {
            //INTERMIDIATE LAYER INSTANCE -- TALKS TO DATA ACCESS LAYER
            EmployeeBL employeeBL = new EmployeeBL();
            messageLabel.Text = "";
            //Validations
            if (fNameTextBox.Text != "" && lNameTextBox.Text != "")
            {
                // int phoneNumber;
                if (phoneTextBox.Text != "")
                {
                    if (eMailTextBox.Text != "")
                    {
                        //DOMAIN LAYER INSTANCE -- CREATE NEW EMPLOYEE INSTANCE -- LOCALLY STORE EMPLOYEE INFO FROM FORM
                        Employee<int, string> temp = new Employee<int, string>(-1, fNameTextBox.Text, lNameTextBox.Text, phoneTextBox.Text, eMailTextBox.Text);

                        //DATA ACCESS LAYER INSTANCE -- PERFORMS INSERT
                        int employees = employeeBL.InsertNewRecord(temp);

                        //OUTPUT MESSAGE TO USER ON FROM
                        messageLabel.Text = $"You have successfully submitted {employees} new employee.";
                    }
                    else
                    {
                        MessageBox.Show("Enter a vaild email id and address!", "Error");
                    }//email and address validation
                }
                else
                {
                    MessageBox.Show("Enter a valid phone number!", "Error");
                }//phoneNumber validation

            }
            else
            {
                MessageBox.Show("Enter a valid customer first name and last name!", "Error");
            }//Name vaildation            
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close(); //closee the form
        }

        //making form singleton
        private void CreateEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            employeeInstance = null;
        }
    }
}
