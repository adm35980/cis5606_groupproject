﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class VehicleInsertionForm : Form
    {
        private static VehicleInsertionForm vehicleInsertionForm;

        public VehicleInsertionForm()
        {
            InitializeComponent();
        }

        internal static VehicleInsertionForm GetVehicleInstance()
        {
            if(vehicleInsertionForm == null)
            {
                vehicleInsertionForm = new VehicleInsertionForm();
            }

            return vehicleInsertionForm;
        }

        private void addVehiclebutton_Click(object sender, EventArgs e)
        {
            /*
             * PERFORMING AN INSERT HERE
             * - NEED TO ACCESS THE DATA ACCESS LAYER THROUGH THE BUSINESS LAYER
             * - NEED TO CREATE AN INSTANCE OF A VEHICLE FROM THE DOMAIN LAYER CLASS
             * - PASS THAT VEHICLE INSTANCE TO THE BUSINESS LAYER, WHICH PASSES IT TO THE DATA ACCESS LAYER AND PERFORMS THE INSERT
             * - DISPLAY SUCCESSFUL INSERTION MESSAGE IN LABEL ON FORM
             * - FINALLY, CHECK DATABASE TO SEE IF ENTRY WAS SUCCESSFUL
            */

            //ERROR EXCEPTION INSTANCIATIONS
            BLException blException = new BLException();

            //STEP 1: ACCESS INTERMEDIATE LAYER INSTANTIATE CLASS
            VehicleBL vehicleBL = new VehicleBL();

            messageLabel.Text = "";

            //validations
            if(vinTextBox.Text!=""&& makeTextBox.Text!="" && modelTextBox.Text!="")
            {
                if(yearComboBox.SelectedIndex!=-1 && colorComboBox.SelectedIndex!=-1 && capacityComboBox.SelectedIndex!=-1)
                {
                    if(mileageTextBox.Text!="" && vehicleTypeComboBox.SelectedIndex!=-1)
                    {
                        //STEP 2: CREATE GENERIC VEHICLE DOMAIN CLASS INSTANCE
                        Vehicle<int, string> tempVehicle = new Vehicle<int, string>(-1, vinTextBox.Text, makeTextBox.Text, modelTextBox.Text,
                                                            Convert.ToInt32(yearComboBox.SelectedItem), colorComboBox.SelectedItem.ToString(),
                                                            Convert.ToInt32(capacityComboBox.SelectedItem), Convert.ToInt32(mileageTextBox.Text),
                                                            vehicleTypeComboBox.SelectedItem.ToString());


                        //messageLabel.Text = $"Vehicle Information: {tempVehicle.ToString()}"; //used for testing purposes

                        //STEP 3: PASS THAT VEHICLE INSTANCE TO THE BUSINESS LAYER
                        int records = vehicleBL.InsertNewRecord(tempVehicle);

                        //STEP 4: DISPLAY MESSAGE ON FORM
                        messageLabel.Text = $"You have successfully inserted {records} new vehicle into the inventory!!!";

                        //messageLabel.Text = blException.ErrorReturned;

                    }
                    else
                    {
                        messageLabel.Text = "Please give a proper for mileage and vehicle type!";
                    }
                }
                else
                {
                    messageLabel.Text = "Please select an appropriate value for year,color and capacity!";
                }
            }
            else
            {
                messageLabel.Text = "Please enter a proper value for VIN, make and model of vehicle to inserted!";
            }
            
        }

        private void VehicleInsertionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            vehicleInsertionForm = null;
        }
    }
}
