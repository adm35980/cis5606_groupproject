﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class EmployeeInformationForm : Form
    {
        //SINGLETONG
        private static EmployeeInformationForm employeeForm;

        internal static EmployeeInformationForm GetEmployeeInstance()
        {
            if(employeeForm == null)
            {
                employeeForm = new EmployeeInformationForm();
            }

            return employeeForm;
        }

        //FORM LOAD
        public EmployeeInformationForm()
        {
            InitializeComponent();
        }

        private void EmployeeInformationForm_Load(object sender, EventArgs e)
        {
            PopulateGridView();

            //GRIDVIEW BUTTONS
            //delete button
            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            deleteButton.Name = "deleteButton";
            deleteButton.HeaderText = ""; //nothing in delete column header
            deleteButton.HeaderText = null;
            deleteButton.Text = "Delete"; //gets and set button text
            deleteButton.UseColumnTextForButtonValue = true; //display text on button
            employeesGridView.Columns.Add(deleteButton); //add button to gridview

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            editButton.Name = "editButton";
            editButton.HeaderText = ""; //nothing in delete column header
            editButton.HeaderText = null;
            editButton.Text = "Edit"; //gets and set button text
            editButton.UseColumnTextForButtonValue = true; //display text on button
            employeesGridView.Columns.Add(editButton); //add button to gridview

        } 

        public void PopulateGridView()
        {
            EmployeeBL employeeBL = new EmployeeBL();
            employeesGridView.DataSource = employeeBL.FetchData();
        }


        //DELETING & UPDATING
        private void employeesGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == employeesGridView.Columns["deleteButton"].Index)
            {
                int id = Convert.ToInt32(employeesGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the VIN column of the row being deleted

                Employee<int, string> tempVehicle = new Employee<int, string>(id, "%", "%", "%", "%"); //create temporary vehicle instance

                EmployeeBL employeeBL = new EmployeeBL(); //pass temporary vehicle instance to intermediate layer for deletion

                int recordsDeleted = employeeBL.DeleteOneRecord(tempVehicle);

                if(recordsDeleted < 1)
                {
                    messageLabel.Text = "Employee Account currently in use in an open rental. Please delete or update all rentals using employee account.";

                }
                else
                {
                    messageLabel.Text = $"You have deleted {recordsDeleted} record(s)!!!";

                }


                PopulateGridView();
            }
            if (e.ColumnIndex == employeesGridView.Columns["editButton"].Index)
            {
                int id = Convert.ToInt32(employeesGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the Id column 
                string fname = employeesGridView.Rows[e.RowIndex].Cells["First_Name"].Value.ToString(); //gets the new value in the VIN column
                string lname = employeesGridView.Rows[e.RowIndex].Cells["Last_Name"].Value.ToString(); //gets the value in the make column
                string phone = employeesGridView.Rows[e.RowIndex].Cells["Phone"].Value.ToString(); //gets the value in the model column
                string email = employeesGridView.Rows[e.RowIndex].Cells["Email"].Value.ToString(); //gets the value in the model column

                idLabel.Text = id.ToString();
                fnameTextBox.Text = fname;
                lnameTextBox.Text = lname;
                phoneTextBox.Text = phone;
                emailTextBox.Text = email;

            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            Employee<int, string> temp = new Employee<int, string>(Convert.ToInt32(idLabel.Text), fnameTextBox.Text, lnameTextBox.Text, phoneTextBox.Text, emailTextBox.Text);

            EmployeeBL employeeBL = new EmployeeBL();
            int records = employeeBL.UpdateOneRecord(temp);

            messageLabel.Text = $"You have successfully updated employee account #{temp.Id}!!!";
            messageLabel.ForeColor = System.Drawing.Color.Green;

            PopulateGridView();

            idLabel.Text = string.Empty;
            fnameTextBox.Text = string.Empty;
            lnameTextBox.Text = string.Empty;
            phoneTextBox.Text = string.Empty;
            emailTextBox.Text = string.Empty;
        }

        private void EmployeeInformationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            employeeForm = null;
        }


        //GRIDVIEW EDITMODE
    }
}
