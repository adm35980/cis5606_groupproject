﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class VehicleInformationForm : Form
    {
        //SINGLETON
        private static VehicleInformationForm vehicleForm;

        internal static VehicleInformationForm GetVehicleInstance()
        {
            if (vehicleForm == null)
            {
                vehicleForm = new VehicleInformationForm();
            }

            return vehicleForm;
        }

        //FORMLOAD
        public VehicleInformationForm()
        {
            InitializeComponent();
        }
       
        private void VehicleInformationForm_Load(object sender, EventArgs e) //TESTS GOOD TO GO -- CAN HIDE FORM NOW
        {
            PopulateGridView();

            //GRIDVIEW BUTTONS
            //delete button
            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            deleteButton.Name = "deleteButton";
            deleteButton.HeaderText = ""; //nothing in delete column header
            deleteButton.HeaderText = null;
            deleteButton.Text = "Delete"; //gets and set button text
            deleteButton.UseColumnTextForButtonValue = true; //display text on button
            vehiclesGridView.Columns.Add(deleteButton); //add button to gridview

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            editButton.Name = "editButton";
            editButton.HeaderText = ""; //nothing in delete column header
            editButton.HeaderText = null;
            editButton.Text = "Edit"; //gets and set button text
            editButton.UseColumnTextForButtonValue = true; //display text on button
            vehiclesGridView.Columns.Add(editButton); //add button to gridview


        } //end load event

        public void PopulateGridView()
        {
            //INSTANTIATE NEW INTERMEDIATE LAYER OBJECT
            VehicleBL vehicleBL = new VehicleBL();
            //Display Data
            vehiclesGridView.DataSource = vehicleBL.FetchData();
        }


        //DELETING & UPDATING
        private void vehiclesGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == vehiclesGridView.Columns["deleteButton"].Index)
            {
                int id = Convert.ToInt32(vehiclesGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the VIN column of the row being deleted

                Vehicle<int, string> tempVehicle = new Vehicle<int, string>(id, "%", "%", "%", -1, "%", -1, -1, "%"); //create temporary vehicle instance

                VehicleBL vehicleBL = new VehicleBL(); //pass temporary vehicle instance to intermediate layer for deletion

                int recordsDeleted = vehicleBL.DeleteOneRecord(tempVehicle);

                if(recordsDeleted < 1)
                {
                    messageLabel.Text = "Vehicle currently in use in an open rental. Please delete or update all rentals using vehicle.";
                }
                else
                {
                    messageLabel.Text = $"You have deleted {recordsDeleted} record(s)!!!";

                }

                PopulateGridView();
            }
            if (e.ColumnIndex == vehiclesGridView.Columns["editButton"].Index)
            {
                int id = Convert.ToInt32(vehiclesGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the Id column 
                string vehicleVIN = vehiclesGridView.Rows[e.RowIndex].Cells["VIN"].Value.ToString(); //gets the new value in the VIN column
                string make = vehiclesGridView.Rows[e.RowIndex].Cells["Make"].Value.ToString(); //gets the value in the make column
                string model = vehiclesGridView.Rows[e.RowIndex].Cells["Model"].Value.ToString(); //gets the value in the model column
                int mileage = Convert.ToInt32(vehiclesGridView.Rows[e.RowIndex].Cells["Mileage"].Value.ToString()); //gets the value in the year column;

                idLabel.Text = id.ToString();
                vinTextBox.Text = vehicleVIN;
                makeTextBox.Text = make;
                modelTextBox.Text = model;
                mileageTextBox.Text = mileage.ToString();

            }
        }

        private void updateRentalButton_Click(object sender, EventArgs e)
        {
            Vehicle<int, string> temp = new Vehicle<int, string>(Convert.ToInt32(idLabel.Text), 
                                                                 vinTextBox.Text, 
                                                                 makeTextBox.Text, 
                                                                 modelTextBox.Text, 
                                                                 Convert.ToInt32(yearComboBox.SelectedItem), 
                                                                 colorComboBox.SelectedItem.ToString(), 
                                                                 Convert.ToInt32(capacityComboBox.SelectedItem), 
                                                                 Convert.ToInt32(mileageTextBox.Text), 
                                                                 typeComboBox.SelectedItem.ToString());

            VehicleBL vehicleBL = new VehicleBL();
            int records = vehicleBL.UpdateOneRecord(temp);

            messageLabel.Text = $"You have successfully updated vehicle #{temp.Id}";
            messageLabel.ForeColor = System.Drawing.Color.Green;

            PopulateGridView();

            idLabel.Text = string.Empty;
            vinTextBox.Text = string.Empty;
            makeTextBox.Text = string.Empty;
            modelTextBox.Text = string.Empty;
            mileageTextBox.Text = string.Empty;
        }

        private void VehicleInformationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            vehicleForm = null;
        }


        //GRIDVIEW EDITMODE

    } //end form
} //end namespace
