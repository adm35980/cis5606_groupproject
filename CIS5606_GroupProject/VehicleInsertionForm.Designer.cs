﻿namespace CIS5606_GroupProject
{
    partial class VehicleInsertionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.makeTextBox = new System.Windows.Forms.TextBox();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.colorComboBox = new System.Windows.Forms.ComboBox();
            this.mileageTextBox = new System.Windows.Forms.TextBox();
            this.vehicleTypeComboBox = new System.Windows.Forms.ComboBox();
            this.addVehiclebutton = new System.Windows.Forms.Button();
            this.yearComboBox = new System.Windows.Forms.ComboBox();
            this.messageLabel = new System.Windows.Forms.Label();
            this.vinTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.capacityComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Goudy Stout", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(75, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vehicle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Make:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Model:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Year:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Color:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 269);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Capacity:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 315);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Mileage:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 372);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Type:";
            // 
            // makeTextBox
            // 
            this.makeTextBox.Location = new System.Drawing.Point(104, 106);
            this.makeTextBox.Name = "makeTextBox";
            this.makeTextBox.Size = new System.Drawing.Size(181, 22);
            this.makeTextBox.TabIndex = 8;
            // 
            // modelTextBox
            // 
            this.modelTextBox.Location = new System.Drawing.Point(104, 145);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(181, 22);
            this.modelTextBox.TabIndex = 9;
            // 
            // colorComboBox
            // 
            this.colorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.colorComboBox.FormattingEnabled = true;
            this.colorComboBox.Items.AddRange(new object[] {
            "Black",
            "White",
            "Silver",
            "Metallic Gray",
            "Red",
            "Orange",
            "Blue",
            "Green",
            ""});
            this.colorComboBox.Location = new System.Drawing.Point(104, 227);
            this.colorComboBox.Name = "colorComboBox";
            this.colorComboBox.Size = new System.Drawing.Size(181, 24);
            this.colorComboBox.TabIndex = 11;
            // 
            // mileageTextBox
            // 
            this.mileageTextBox.Location = new System.Drawing.Point(104, 315);
            this.mileageTextBox.Name = "mileageTextBox";
            this.mileageTextBox.Size = new System.Drawing.Size(181, 22);
            this.mileageTextBox.TabIndex = 13;
            // 
            // vehicleTypeComboBox
            // 
            this.vehicleTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.vehicleTypeComboBox.FormattingEnabled = true;
            this.vehicleTypeComboBox.Items.AddRange(new object[] {
            "Sedan",
            "SUV",
            "Hatchback",
            "Pickup",
            "Van"});
            this.vehicleTypeComboBox.Location = new System.Drawing.Point(104, 365);
            this.vehicleTypeComboBox.Name = "vehicleTypeComboBox";
            this.vehicleTypeComboBox.Size = new System.Drawing.Size(181, 24);
            this.vehicleTypeComboBox.TabIndex = 14;
            // 
            // addVehiclebutton
            // 
            this.addVehiclebutton.Location = new System.Drawing.Point(126, 418);
            this.addVehiclebutton.Name = "addVehiclebutton";
            this.addVehiclebutton.Size = new System.Drawing.Size(159, 27);
            this.addVehiclebutton.TabIndex = 15;
            this.addVehiclebutton.Text = "Add To Inventory";
            this.addVehiclebutton.UseVisualStyleBackColor = true;
            this.addVehiclebutton.Click += new System.EventHandler(this.addVehiclebutton_Click);
            // 
            // yearComboBox
            // 
            this.yearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.yearComboBox.FormattingEnabled = true;
            this.yearComboBox.Items.AddRange(new object[] {
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021"});
            this.yearComboBox.Location = new System.Drawing.Point(104, 183);
            this.yearComboBox.Name = "yearComboBox";
            this.yearComboBox.Size = new System.Drawing.Size(181, 24);
            this.yearComboBox.TabIndex = 16;
            // 
            // messageLabel
            // 
            this.messageLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.messageLabel.Location = new System.Drawing.Point(14, 463);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(365, 82);
            this.messageLabel.TabIndex = 17;
            // 
            // vinTextBox
            // 
            this.vinTextBox.Location = new System.Drawing.Point(104, 69);
            this.vinTextBox.Name = "vinTextBox";
            this.vinTextBox.Size = new System.Drawing.Size(181, 22);
            this.vinTextBox.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "VIN #:";
            // 
            // capacityComboBox
            // 
            this.capacityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.capacityComboBox.FormattingEnabled = true;
            this.capacityComboBox.Items.AddRange(new object[] {
            "2",
            "4",
            "5",
            "6",
            "8"});
            this.capacityComboBox.Location = new System.Drawing.Point(105, 269);
            this.capacityComboBox.Name = "capacityComboBox";
            this.capacityComboBox.Size = new System.Drawing.Size(181, 24);
            this.capacityComboBox.TabIndex = 20;
            // 
            // VehicleInsertionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 562);
            this.Controls.Add(this.capacityComboBox);
            this.Controls.Add(this.vinTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.yearComboBox);
            this.Controls.Add(this.addVehiclebutton);
            this.Controls.Add(this.vehicleTypeComboBox);
            this.Controls.Add(this.mileageTextBox);
            this.Controls.Add(this.colorComboBox);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(this.makeTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "VehicleInsertionForm";
            this.Text = "Vehicle Test Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VehicleInsertionForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox makeTextBox;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.ComboBox colorComboBox;
        private System.Windows.Forms.TextBox mileageTextBox;
        private System.Windows.Forms.ComboBox vehicleTypeComboBox;
        private System.Windows.Forms.Button addVehiclebutton;
        private System.Windows.Forms.ComboBox yearComboBox;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.TextBox vinTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox capacityComboBox;
    }
}

