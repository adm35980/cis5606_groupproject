﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS5606_GroupProject
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //closes the mainForm - whole application
            this.Close();
        }//exitToolStrip

        private void createRentalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Instantiate RentalForm
            CreateRental rentalForm = CreateRental.GetRentalInstance();
            //make rental Form Main Form's child
            rentalForm.MdiParent = this;
            //Show the form
            rentalForm.Show();
        }

        private void createCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateCustomerAccount createAccount = CreateCustomerAccount.createCustomerAccount();
            createAccount.MdiParent = this;
            createAccount.Show();
        }

        private void createEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateEmployee createEmployee = CreateEmployee.CreateEmployeeInstance();
            createEmployee.MdiParent = this;
            createEmployee.Show();
        }

        private void vehicleInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VehicleInformationForm vehicleInformationForm = VehicleInformationForm.GetVehicleInstance();
            vehicleInformationForm.MdiParent = this;
            vehicleInformationForm.Show();
        }

        private void employeeInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeInformationForm employeeInformationForm = EmployeeInformationForm.GetEmployeeInstance();
            employeeInformationForm.MdiParent = this;
            employeeInformationForm.Show();
        }

        private void customerInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerInformationForm customerInformationForm = CustomerInformationForm.GetCustomerInstance();
            customerInformationForm.MdiParent = this;
            customerInformationForm.Show();
        }

        private void rentalInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RentalsInformationForm rentalsInformationForm = RentalsInformationForm.GetRentalsInstance();
            rentalsInformationForm.MdiParent = this;
            rentalsInformationForm.Show();
        }

        private void HomeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HomePage homePage = HomePage.GetHomePageInstance();
            homePage.MdiParent = this;
            homePage.Show();
        }

        private void createVehicleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VehicleInsertionForm vehicleInsertionForm = VehicleInsertionForm.GetVehicleInstance();
            vehicleInsertionForm.MdiParent = this;
            vehicleInsertionForm.Show();
        }
    }
}
