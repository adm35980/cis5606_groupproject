﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class RentalsInformationForm : Form
    {
        //SINGLETON
        private static RentalsInformationForm rentalsInformationForm;
        
        internal static RentalsInformationForm GetRentalsInstance()
        {
            if(rentalsInformationForm == null)
            {
                rentalsInformationForm = new RentalsInformationForm();
            }
            return rentalsInformationForm;
        }

        //FORM LOAD
        public RentalsInformationForm()
        {
            InitializeComponent();
        }

        private void RentalsInformationForm_Load(object sender, EventArgs e)
        {
            PopulateGridView();

            //GRIDVIEW BUTTONS
            //delete button
            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            deleteButton.Name = "deleteButton";
            deleteButton.HeaderText = ""; //nothing in delete column header
            deleteButton.HeaderText = null;
            deleteButton.Text = "Delete"; //gets and set button text
            deleteButton.UseColumnTextForButtonValue = true; //display text on button
            rentalsDataGridView.Columns.Add(deleteButton); //add button to gridview

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn(); //new DataGridViewButtonColumn instance
            editButton.Name = "editButton";
            editButton.HeaderText = ""; //nothing in delete column header
            editButton.HeaderText = null;
            editButton.Text = "Edit"; //gets and set button text
            editButton.UseColumnTextForButtonValue = true; //display text on button
            rentalsDataGridView.Columns.Add(editButton); //add button to gridview

            PopulateDropDowns();


           
        }

        public void PopulateDropDowns()
        {
            //Populate Dropdowns and fill in labels
            VehicleBL vehicleBL = new VehicleBL();
            DataTable vehicleInfo = vehicleBL.FetchData();
            BindingSource vehicleBindingSource = new BindingSource();
            vehicleBindingSource.DataSource = vehicleInfo;

            //setup binding source up
            vehicleIdComboBox.DataSource = vehicleBindingSource;
            vehicleIdComboBox.ValueMember = "Id";
            vehicleIdComboBox.DisplayMember = Convert.ToString("Id");
            vehicleIdComboBox.DataBindings.Add("text", vehicleBindingSource, "Id", false, DataSourceUpdateMode.Never);

            vinLabel.DataBindings.Add("text", vehicleBindingSource, "VIN", false);
            makeLabel.DataBindings.Add("text", vehicleBindingSource, "Make", false);
            modelLabel.DataBindings.Add("text", vehicleBindingSource, "Model", false);
        }

        public void PopulateGridView()
        {
            RentalBL rentalBL = new RentalBL();
            rentalsDataGridView.DataSource = rentalBL.FetchData();

            //RENAMED COLUMN HEADERS BC ALL 4 WILL SAY EITHER FIRST NAME OR LAST NAME -- NEED TO DISTINGUISH BETWEEN CUSTOMER AND EMPLOYEE 
            rentalsDataGridView.Columns[6].HeaderText = "Customer First Name";
            rentalsDataGridView.Columns[7].HeaderText = "Customer Last Name";
            rentalsDataGridView.Columns[8].HeaderText = "Employee First Name";
            rentalsDataGridView.Columns[9].HeaderText = "Employee First Name";
            rentalsDataGridView.Columns[2].HeaderText = "Duration (Days)"; //let employee know duration is in days -- specifying units

        }


        //DELETING ACTION & UPDATE FILL (FILL IN LABELS AND DROPDWONS FOR UPDATING)
        private void rentalsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == rentalsDataGridView.Columns["deleteButton"].Index)
            {
                int id = Convert.ToInt32(rentalsDataGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the VIN column of the row being deleted

                Rental<int, string> tempRental = new Rental<int, string>(id, "%", -1, -1, -1, -1); //create temporary vehicle instance

                RentalBL rentalBL = new RentalBL(); //pass temporary vehicle instance to intermediate layer for deletion

                int recordsDeleted = rentalBL.DeleteOneRecord(tempRental);

                messageLabel.Text = $"You have deleted {recordsDeleted} record(s)!!!";

                PopulateGridView();
            }
            if (e.ColumnIndex == rentalsDataGridView.Columns["editButton"].Index)
            {
                int id = Convert.ToInt32(rentalsDataGridView.Rows[e.RowIndex].Cells["Id"].Value.ToString()); //gets the value in the Id column 

                

                    //fill in rental id label to remind user which rental they are updating
                    rentalIDLabel.Text = id.ToString();
                
            }
        }

        private void updateRentalButton_Click(object sender, EventArgs e)
        {
            messageLabel.Text = string.Empty;

            Rental<int, string> temp = new Rental<int, string>(Convert.ToInt32(rentalIDLabel.Text),
                                                                  Convert.ToString(rentalDateTimePicker.Value),
                                                                  Convert.ToInt32(durationComboBox.SelectedItem),
                                                                  Convert.ToInt32(vehicleIdComboBox.SelectedValue),
                                                                  Convert.ToInt32(null),
                                                                  Convert.ToInt32(null));
            //messageLabel.Text = $"{temp.ToString()}";

            RentalBL rentalBL = new RentalBL();
            int records = rentalBL.UpdateOneRecord(temp);

            messageLabel.Text = $"You have successfully updated rental #{temp.Id}!!!";
            messageLabel.ForeColor = System.Drawing.Color.Green;

            PopulateGridView();

        }

        private void RentalsInformationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            rentalsInformationForm = null;
        }
    }
}
