﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS5606.BusinessLayer;
using CIS5606.DomainLayer;

namespace CIS5606_GroupProject
{
    public partial class CreateCustomerAccount : Form
    {
        //SINGLETON FROM ITEMS
        private static CreateCustomerAccount customerAccount;
        //private ctor
        private CreateCustomerAccount() 
        {
            InitializeComponent();
        }

        //Making the form singleton
        internal static CreateCustomerAccount createCustomerAccount()
        {
            if(customerAccount == null)
            {
                customerAccount = new CreateCustomerAccount();
            }
            return customerAccount;
        }

        //FORM FUNCTIONALITY 
        private void accountSubmitButton_Click(object sender, EventArgs e)
        {
            /*
            * PERFORMING AN INSERT HERE
            * - NEED TO ACCESS THE DATA ACCESS LAYER THROUGH THE BUSINESS LAYER
            * - NEED TO CREATE AN INSTANCE OF A RENTAL FROM THE DOMAIN LAYER CLASS
            * - PASS THAT RENTAL INSTANCE TO THE BUSINESS LAYER, WHICH PASSES IT TO THE DATA ACCESS LAYER AND PERFORMS THE INSERT
            * - DISPLAY SUCCESSFUL INSERTION MESSAGE IN LABEL ON FORM
            * - FINALLY, CHECK DATABASE TO SEE IF ENTRY WAS SUCCESSFUL
           */

            //ERROR EXCEPTION INSTANCIATION
            BLException blException = new BLException();

            //STEP 1: ACCESS INTERMEDIATE LAYER INSTANTIATE CLASS
            CustomerBL customerBL = new CustomerBL();

            messageLabel.Text = "";

            //STEP 2: CREATE CUSTMER DOMAIN CLASS INSTANCE
            if(fNameTextBox.Text!=""&&lNameTextBox.Text!="")
            {
               // int phoneNumber;
                if(phoneNumberTextBox.Text!="")
                {
                    if(eMailTextBox.Text!=""&&homeAddresstextBox.Text!="")
                    {
                        Customer<int, string> tempCustomer = new Customer<int, string>(-1, fNameTextBox.Text, lNameTextBox.Text, phoneNumberTextBox.Text, eMailTextBox.Text, homeAddresstextBox.Text);

                        //STEP 3: PASS THAT CUSTOMER INSTANCE TO THE BUSINESS LAYER AND STORE NUMBER OF ROWS AFFECTED IN LOCAL 'CUSTOMERS' VARIABLE
                        int customers = customerBL.InsertNewRecord(tempCustomer);

                        //STEP 4: DISPLAY MESSAGE ON FORM
                        messageLabel.Text += $"You have successfully created {customers} new customer account!!!";
                    }
                    else
                    {
                        MessageBox.Show("Enter a vaild email id and address!", "Error");
                    }//email and address validation
                }
                else
                {
                    MessageBox.Show("Enter a valid phone number!", "Error");
                }//phoneNumber validation

            }
            else
            {
                MessageBox.Show("Enter a valid customer first name and last name!", "Error");
            }//Name vaildation
        }
            

        private void accountExitButton_Click(object sender, EventArgs e)
        {
            this.Close(); //closes the form
        }

        private void CreateCustomerAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            customerAccount = null;
        }
    }
}
